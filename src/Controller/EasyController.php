<?php

namespace App\Controller;

use App\Entity\Entreprise;
use App\Entity\Eleve;
use App\Entity\Prof;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use App\Form\UtilisateurType;
use App\Entity\Utilisateur;
use App\Entity\Stage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class EasyController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        return $this->render('easy/index.html.twig');
    }
                            ////////Fonction entreprise//////////
    /**
     * @Route("/liste/entreprise", name="listeEntreprise")
     */
    public function listeEntreprise()
    {
        $repository = $this->getDoctrine()->getRepository(Entreprise::class);
        $entreprise = $repository->findAll();

        return $this->render('easy/listeEntreprise.html.twig', compact("entreprise"));
    }

    /**
     * @Route("/liste/entreprise/{id}", name="detailE")
     */
    public function detailEntreprise($id)
    {
        $repository = $this->getDoctrine()->getRepository(Entreprise::class);
        $entreprise = $repository->find($id);

        return $this->render('easy/detailEntreprise.html.twig', compact("entreprise"));
    }
    /**
     * @Route("/add/entreprise", name="addEntreprise")
     */
    public function AddEntreprise(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $entreprise = new Entreprise();
        $entreprise->setActive(1);

        $entreprise = $this->createFormBuilder($entreprise)
            ->add('nom', TextType::class)
            ->add('mail', TextType::class, array('label' => 'mail','required' => false))
            ->add('telephone', TextType::class)
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('cp', TextType::class)
            ->add('activite', TextType::class)
            ->add('Ajouter', SubmitType::class, array('label' => 'Ajouter une entreprise'))
            ->getForm();

        $entreprise->handleRequest($request);

        if ($entreprise->isSubmitted() && $entreprise->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $entreprise = $entreprise->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entreprise);
            $entityManager->flush();

            return $this->redirectToRoute('listeEntreprise');
        }
        return $this->render('easy/addEntreprise.html.twig', array(
            'form' => $entreprise->createView(),
        ));
    }

    /**
     * @Route("/entreprise/delete/{id}", name="deleteEntreprise")
     */
    public function deleteEntreprise($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entreprise = $entityManager->getRepository(Entreprise::class)->find($id);

        if (!$entreprise) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        $entityManager->remove($entreprise);
        $entityManager->flush();

        return $this->redirectToRoute('listeEntreprise');
    }
    /**
     * @Route("/entreprise/modify/{id}", name="modifyEntreprise")
     */
    public function modifyEntreprise($id, Request $request)
    {

        $repository = $this->getDoctrine()->getRepository(Entreprise::class);
        $entreprise = $repository->find($id);
        $form = $this->createFormBuilder($entreprise)
            ->add('nom', TextType::class)
            ->add('mail', TextType::class, array('label' => 'mail','required' => false))
            ->add('telephone', TextType::class)
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('cp', TextType::class)
            ->add('activite', TextType::class)
            ->add('modifier', SubmitType::class, array('label' => 'Modifier entreprise'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entreprise = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($entreprise);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('easy/modifyEntreprise.html.twig', array(
            'form' => $form->createView(),
        ));
    }
                        ///////fonction eleve//////////
    /**
     * @Route("/liste/eleve", name="listeEleve")
     */
    public function listeEleve()
    {
        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleve = $repository->findAll();

        return $this->render('easy/listeEleve.html.twig', compact("eleve"));
    }

    /**
     * @Route("/liste/eleve/{id}", name="detailEleve")
     */
    public function detailEleve($id)
    {
        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleve = $repository->find($id);

        return $this->render('easy/detailEleve.html.twig', compact("eleve"));
    }
    /**
     * @Route("/add/eleve", name="addEleve")
     */
    public function AddEleve(Request $request)
    {
        // creates a task and gives it some dummy data for this example
        $eleve = new Eleve();

        $eleve = $this->createFormBuilder($eleve)
            ->add('nom', TextType::class)
            ->add('datenaissance',BirthdayType::class)
            ->add('mail', TextType::class, array('label' => 'mail','required' => false))
            ->add('telephone', TextType::class)
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('Ajouter', SubmitType::class, array('label' => 'Ajouter un eleve'))
            ->getForm();

        $eleve->handleRequest($request);

        if ($eleve->isSubmitted() && $eleve->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $eleve = $eleve->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eleve);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('easy/addEleve.html.twig', array(
            'form' => $eleve->createView(),
        ));
    }
    /**
     * @Route("/eleve/delete/{id}", name="deleteEleve")
     */
    public function deleteEleve($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $eleve = $entityManager->getRepository(Eleve::class)->find($id);

        if (!$eleve) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }

        $entityManager->remove($eleve);
        $entityManager->flush();

        return $this->redirectToRoute('listeEleve');
    }
    /**
     * @Route("/eleve/modify/{id}", name="modifyEleve")
     */
    public function modifyEleve($id, Request $request)
    {

        $repository = $this->getDoctrine()->getRepository(Eleve::class);
        $eleve = $repository->find($id);
        $form = $this->createFormBuilder($eleve)
            ->add('nom', TextType::class)
            ->add('mail', TextType::class, array('label' => 'mail','required' => false))
            ->add('telephone', TextType::class, array('label' => 'telephone','required' => false))
            ->add('ville', TextType::class)
            ->add('adresse', TextType::class)
            ->add('modifier', SubmitType::class, array('label' => 'Modifier eleve'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $eleve = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($eleve);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }

        return $this->render('easy/modifyEleve.html.twig', array(
            'form' => $form->createView(),
        ));
    }
                    ///////fonction prof////////
    /**
     * /**
     * @Route("/liste/prof", name="listeProf")
     */
    public function listeProf()
    {
        $repository = $this->getDoctrine()->getRepository(Prof::class);
        $prof = $repository->findAll();

        return $this->render('easy/listeProf.html.twig', compact("prof"));
    }

    /**
     * @Route("/liste/prof/{id}", name="detailProf")
     */
    public function detailProf($id)
    {
        $repository = $this->getDoctrine()->getRepository(Prof::class);
        $prof = $repository->find($id);

        return $this->render('easy/detailProf.html.twig', compact("prof"));
    }

            /////////////////Inscription/////////////////

    /**
     * @Route("/inscription", name= "inscription")
     */
    public function inscriptionAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // 1) build the form
        $user = new Utilisateur();
        $form = $this->createForm(UtilisateurType::class, $user);
        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            //on active par défaut
            $user->setIsActive(true);
            $user->addRole("ROLE_USER");
            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user
            $this->addFlash('success', 'Votre compte à bien été enregistré.');
            return $this->redirectToRoute('login');
        }
        return $this->render('easy/inscription.html.twig', ['form' => $form->createView(),
            'mainNavRegistration' => true, 'title' => 'Inscription']);

    }

            ////////////////Login/////////////
    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils) {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        //
        $form = $this->get('form.factory')
            ->createNamedBuilder(null)
            ->add('_username', null, ['label' => 'Email'])
            ->add('_password', \Symfony\Component\Form\Extension\Core\Type\PasswordType::class,
                ['label' => 'Mot de passe'])
            ->add('ok', \Symfony\Component\Form\Extension\Core\Type\SubmitType::class, ['label' =>
                'Ok', 'attr' => ['class' => 'btn-primary btn-block']])
            ->getForm();
        return $this->render('easy/login.html.twig', [
            'mainNavLogin' => true, 'title' => 'Connexion',
            //
            'form' => $form->createView(),
            'last_username' => $lastUsername,
            'error' => $error,
        ]);
    }
            ///////////Stage///////////////

    /**
     * @Route("/add/stage", name="addStage")
     */
    public function AddStage(Request $request)
    {

        $stage = new Stage();

        $stage = $this->createFormBuilder($stage)
            ->add('eleve', TextType::class)
            ->add('entreprise',TextType::class)
            ->add('prof', TextType::class, array('label' => 'prof','required' => false))
            ->add('horairedebut', TextType::class)
            ->add('horairefin', TextType::class)
            ->add('dateexecution', BirthdayType::class)
            ->add('Ajouter', SubmitType::class, array('label' => 'Ajouter un stage'))
            ->getForm();

        $stage->handleRequest($request);

        if ($stage->isSubmitted() && $stage->isValid()) {
            // $form->getData() holds the submitted values
            // but, the original `$task` variable has also been updated
            $stage = $stage->getData();

            // ... perform some action, such as saving the task to the database
            // for example, if Task is a Doctrine entity, save it!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($stage);
            $entityManager->flush();

            return $this->redirectToRoute('index');
        }
        return $this->render('easy/addStage.html.twig', array(
            'form' => $stage->createView(),
        ));
    }
}
