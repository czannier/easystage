<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
class EleveController extends AbstractController
{
    /**
     * @Route("/eleve", name="eleve")
     */
    public function index()
    {
        return $this->render('eleve/index.html.twig', ['mainNavMember'=>true, 'controller_name' =>
            'Utilisateur', 'title'=>'Espace eleve']);
    }
}
