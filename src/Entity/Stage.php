<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 */
class Stage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="float")
     */
    private $horaireDebut;

    /**
     * @ORM\Column(type="float")
     */
    private $horaireFin;

    /**
     * @ORM\Column(type="date")
     */
    private $dateExecution;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prof", inversedBy="stages")
     * @ORM\JoinColumn(nullable=true)
     */
    private $prof;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Eleve", cascade={"persist", "remove"})
     */
    private $eleve;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise", inversedBy="stages")
     */
    private $entreprise;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHoraireDebut(): ?float
    {
        return $this->horaireDebut;
    }

    public function setHoraireDebut(float $horaireDebut): self
    {
        $this->horaireDebut = $horaireDebut;

        return $this;
    }

    public function getHoraireFin(): ?float
    {
        return $this->horaireFin;
    }

    public function setHoraireFin(float $horaireFin): self
    {
        $this->horaireFin = $horaireFin;

        return $this;
    }

    public function getDateExecution(): ?\DateTimeInterface
    {
        return $this->dateExecution;
    }

    public function setDateExecution(\DateTimeInterface $dateExecution): self
    {
        $this->dateExecution = $dateExecution;

        return $this;
    }

    public function getProf(): ?int
    {
        return $this->prof;
    }

    public function setProf(int $prof): self
    {
        $this->prof = $prof;

        return $this;
    }

    public function getEleve(): ?int
    {
        return $this->eleve;
    }

    public function setEleve(int $eleve): self
    {
        $this->eleve = $eleve;

        return $this;
    }

    public function getEntreprise(): ?int
    {
        return $this->entreprise;
    }

    public function setEntreprise(int $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

}
